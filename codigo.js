            var xmlhttp;

            if (window.XMLHttpRequest) {
                xmlhttp = new XMLHttpRequest();               
            }           
            else {               
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");               
            }

            xmlhttp.onreadystatechange = function () {               
                if (xmlhttp.readyState == 4) {
                    /// Se otienen todas las lineas del fichero.
                    var lineas = xmlhttp.responseText;
                    CodificaDatos(lineas);                 
                    }               
            }

            xmlhttp.open("GET", '/datos.js', true);
            xmlhttp.send();    

        });
        var mediaFacturado = 0;
        var nombreMaxima = 0, nombreMinima = 0;
        var facturacionMinima = 0, facturacionMaxima = 0;
        var primeraEntrada = true;

        function CodificaDatos(lineas) {
            var id;
            var nombre;
            var email;
            var facturado;
            var localidad;
            var totalFacturacion = 0;
            var arrayLineas = lineas.split('\n');
            var totalFilas;
            var datosCodificados = [];
            var nombreMedia, nombreTotal;
            totalFilas = arrayLineas.length;
            
            // Se recorre cada linea del fichero para codificar los datos.
            for (var i = 0; i < totalFilas; i++) {

                // Se obtienen los datos de cada linea
                 id = arrayLineas[i].split(',')[0];
                 nombre = arrayLineas[i].split(',')[1];
                 email = arrayLineas[i].split(',')[2];
                 facturado = arrayLineas[i].split(',')[3];               
                 localidad = arrayLineas[i].split(',')[4];

                // Obtiene el minimo y maximo de la longitud del nombre
                 ObtenerExtremosNombre(nombre, primeraEntrada);

                // Se comprueba si la facturacion tiene valor
                 if (isNaN(parseFloat(facturado)))
                     facturado = 0;
                 else {
                     facturado = parseFloat(facturado);
                 }

                // Obtiene el minimo y maximo de la facturacion
                 ObtenerExtremosFacturacion(facturado, primeraEntrada);
                 primeraEntrada = false;
                

                // Se incrementa el valor del total de la facturacion
                totalFacturacion += facturado;
                nombreTotal += nombre.length;
                // Se añade el elemento codificado al array final
                datosCodificados.push({
                    id: id,
                    nombre: ObtenerCampoCodificado(nombre),
                    email: ObtenerCampoCodificado(email),
                    facturado: 0,
                    localidad: localidad
                });
            }
            mediaFacturado = totalFacturacion / totalFilas;
            datosCodificados.forEach(codificarFacturacion, this);
            nombreMedia = nombreTotal / totalFilas;
            console.log('array final: ' + JSON.stringify(datosCodificados));
            console.log('nombreMinima: ' + nombreMinima);
            console.log('nombreMaxima: ' + nombreMaxima);
            console.log('facturacionMinima: ' + facturacionMinima);
            console.log('facturacionMaxima: ' + facturacionMaxima);
        }

        /// Obtiene el valor codificado
        function ObtenerCampoCodificado(valor) {

            var caracteresEspeciales = ["@", ".", "-"];
            var nuevoValor = '';
            for (var j = 0; j < valor.length; j++) {
                if (caracteresEspeciales.indexOf(valor[j]) == -1) {
                    nuevoValor += "X";
                } else {
                    nuevoValor += valor[j];
                }
            }
            return nuevoValor;
        }

        /// Codifica el valor de la facturacion
        function codificarFacturacion(item, index, arrayDatos) {
            arrayDatos[index].facturado = mediaFacturado;
            //console.log(arrayDatos[index]);
        }

        function ObtenerExtremosNombre(nombre) {
            if (primeraEntrada){
                nombreMinima = nombre.length;
                nombreMaxima = nombre.length;
            }else{
                if (nombre.length < nombreMinima)
                    nombreMinima = nombre.length;
                if (nombre.length > nombreMaxima)
                    nombreMaxima = nombre.length;
            }
        }

        function ObtenerExtremosFacturacion(facturado) {
            if (primeraEntrada) {
                facturacionMinima = facturado;
                facturacionMaxima = facturado;
            } else {
                if (facturado < facturacionMinima)
                    facturacionMinima = facturado;
                if (facturado > facturacionMaxima)
                    facturacionMaxima = facturado;
            }
        }